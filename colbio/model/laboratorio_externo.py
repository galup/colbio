# -*- coding: utf-8 -*-
from odoo import models, fields, api, _

class colbio_laboratorio_externo(models.Model):

	_name= 'colbio.laboratorio.externo'

	name = fields.Char(string='Laboratorio externo')

	_sql_constraints = [
		('name_uniq', 'UNIQUE (name)',  'Ya existe un laboratorio externo con el mismo nombre')
	]