# -*- encoding: utf-8 -*-
###########################################################################
#    Module Writen to OpenERP, Open Source Management Solution
#
#    All Rights Reserved.
#
############################################################################
#
############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    "name": "ColBio",
    "version": "1.0",
    "author": "GALUP Sistemas",
    "description": """
        Módulo para colegio de bioquimicos
    """,
    "license": "AGPL-3",
    "depends": ['account_invoicing',
                'partner_identification',
                'l10n_ar_partner',
                'colbio_backend_theme',
                'report_aeroo',
                'l10n_ar_account',
                'l10n_ar_afipws_fe',
                'l10n_ar_chart',
               ],
    "data": [
        "security/colbio_security.xml",
        "security/ir.model.access.csv",
        "wizard/suspension_wizard_view.xml",
        "wizard/baja_wizard_view.xml",
        "wizard/advertencia_laboratorio_view.xml",
        "view/res_partner_view.xml",
        "wizard/report_matricualdo_wizard.xml",
	    "wizard/report_matricualdo_contacto_wizard.xml",
        "wizard/report_laboratorio_wizard.xml",
        "wizard/fees_wizard.xml",
        "wizard/report_fees.xml",
        "view/ciudad_view.xml",
        "view/universidad_view.xml",
        "view/estudio_titulo_view.xml",
        "view/account_invoice_view.xml",
        "view/idioma_view.xml",
        "view/motivo_view.xml",
        "view/empleo_view.xml",
        "view/profesion_view.xml",
        "data/dias_semana.xml",
        "view/sector_laboratorio_view.xml",
        "view/practica_laboratorio_view.xml",
        "view/tipo_instrumento_laboratorio_view.xml",
        "view/marca_instrumento_laboratorio_view.xml",
        "view/instrumento_laboratorio_view.xml",
        "view/obra_social_view.xml",
        "view/caja_jubilatoria_view.xml",
        "view/product_template.xml",
        "data/matricula_number_sequence.xml",
        "reports/report_matriculado_view.xml",
        "reports/report_matriculado_template.xml",
        "reports/report_matriculado_contacto_view.xml",
        "reports/report_matriculado_contacto_template.xml",
        "reports/report_carnet.xml",
        "reports/report_laboratorio_view.xml",
        "reports/report_laboratorio_template.xml",
        "reports/report_fees_view.xml",
        "reports/report_fees_template.xml",
        # "data/state_data.xml",
        "data/city_data.xml",
        "data/manager_database.xml",
        "email/aviso_deuda_mail_cron.xml",
        "email/aviso_deuda_template.xml",
        "view/menues.xml",
    ],
    'qweb': [],
    "installable": True,
    "active": False,
}
