# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

#Se definen las obras sociales para los matriculados
class colbio_obra_social(models.Model):

    _name= 'colbio.obra.social'

    name = fields.Char(string='Obra Social', required=True)
    prepaga = fields.Selection(string='Prepaga', selection=[('si', 'Si'),
    													('no', 'No')],
                                required=True)