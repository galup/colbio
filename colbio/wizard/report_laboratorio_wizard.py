# -*- coding: utf-8 -*-
# --------------------------------------------------------------------------
#
#Reporte que imprime un listado de los matriculados según su estado de activo, suspendido, baja
#
# ---------------------------------------------------------------------------

from odoo import models, fields, api



class ReportLaboratorioWizard(models.TransientModel):
    _name = 'laboratorio.report.wizard'

    ciudad_view = fields.Many2one('colbio.ciudad', string='Ciudad')
    todas_las_ciudades = fields.Boolean('todas_las_ciudades', default=False)
    state_view = fields.Selection([('todos', 'Todos'),
                             ('pendiente','Pendiente'),
                             ('activo','Activo'),
                             ('suspendido','Suspendido'),
                             ('advertido','Advertido'),
                             ('baja','Baja')], string='Status', default='activo', required=True)

    @api.multi
    def get_report(self):
        data = {
            'ids': self.ids,
            'model': 'self._name',
            'form': {
                'ciudad_view': self.ciudad_view.id,
                'todas_las_ciudades' : self.todas_las_ciudades,
                'state_view' : self.state_view
            },

        }
        return self.env.ref('colbio.reporte_laboratorio').report_action(self, data=data)

class ReportAttendanceColbio(models.AbstractModel):
    """Abstract Model for report template.
    for `_name` model, please use `report.` as prefix then add `module_name.report_name`.
    """

    _name = 'report.colbio.report_laboratorio'

    @api.model
    def get_report_values(self, docids, data=None):
        todas_las_ciudades = data['form']['todas_las_ciudades']
        ciudad_view = data['form']['ciudad_view']
        state_view = data['form']['state_view']
        title = "Reporte de Laboratorios"
        docs = []
        if (todas_las_ciudades):
            if (state_view == 'todos'):
                laboratorios = self.env['res.partner'].search([('is_laboratory', '=', True)], order='city asc')
            else:
                laboratorios = self.env['res.partner'].search([('is_laboratory', '=', True),('state','=',state_view)], order='city asc')
        else:
            if (state_view == 'todos'):
                laboratorios = self.env['res.partner'].search([('is_laboratory', '=', True),('city','=', ciudad_view)],  order='name asc')
            else:
                laboratorios = self.env['res.partner'].search([('is_laboratory', '=', True),('city','=', ciudad_view), ('state','=',state_view)],  order='name asc')
        return {
            'doc_ids': data['ids'],
            'doc_model': data['model'],
            'docs': laboratorios,
            'state_view' : state_view,
            'title': title
        }
