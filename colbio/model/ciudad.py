# -*- coding: utf-8 -*-
from odoo import models, fields, api, _

class colbio_ciudad(models.Model):

	_name= 'colbio.ciudad'
			   
	name = fields.Char(string='Ciudad', required=True)
	state_id = fields.Many2one('res.country.state', string='Provincia',required=True)
	codigo_postal = fields.Integer(string='Código Postal')

	_sql_constraints = [
		('name_uniq', 'UNIQUE (name)',  'Ya existe una ciudad con el mismo nombre')
	]