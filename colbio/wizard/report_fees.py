# -*- coding: utf-8 -*-
# --------------------------------------------------------------------------
#
#Reporte que imprime un listado de los matriculados según su estado de activo, suspendido, baja
#
# ---------------------------------------------------------------------------

from odoo import models, fields, api
import re
from calendar import monthrange
from datetime import datetime, timedelta
from odoo.tools.translate import _
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, \
    DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import except_orm
from odoo.exceptions import ValidationError
from dateutil.relativedelta import relativedelta



from odoo import models, fields, api



class ReportFeesWizard(models.TransientModel):
    _name = 'fees.report.wizard'

    select_type = fields.Selection(string ="Facturar a:" ,selection=[('Matriculados', 'Matriculados'), ('Laboratorios','Laboratorios'), ('Todos','Todos')], default='Todos', required=True)
    todas_las_ciudades = fields.Boolean('todas_las_ciudades', default=False)
    ciudad_view = fields.Many2one('colbio.ciudad', string='Ciudad')
    state_view = fields.Selection([('todos', 'Todos'),
                             ('pendiente','Pendiente'),
                             ('activo','Activo'),
                             ('suspendido','Suspendido'),
                             ('advertido','Advertido'),
                             ('baja','Baja')], string='Status', default='todos', required=True)
    start_date = fields.Date(string="Fecha desde:")
    total_history = fields.Boolean("Todos los tiempos", default=False)

    @api.multi
    def get_report(self):
        data = {
            'ids': self.ids,
            'model': 'self._name',
            'form': {
                'select_type': self.select_type,
                'ciudad_view': self.ciudad_view.id,
                'todas_las_ciudades' : self.todas_las_ciudades,
                'state_view' : self.state_view,
                'start_date' : self.start_date,
                'total_history' : self.total_history,
            },

        }
        return self.env.ref('colbio.reporte_fees').report_action(self, data=data)

class ReportAttendanceColbio(models.AbstractModel):
    """Abstract Model for report template.
    for `_name` model, please use `report.` as prefix then add `module_name.report_name`.
    """

    _name = 'report.colbio.report_fees'

    
    @api.multi
    def getPartner (self,select_type,todas_las_ciudades,ciudad_view):
        if (todas_las_ciudades):
            state_view =""  
        if not (ciudad_view):
            todas_las_ciudades = True
        if (select_type == 'Todos'):
            if (todas_las_ciudades):
                partner_ids = self.env['res.partner'].search(['|',('is_laboratory', '=', True),('is_matriculado', '=', True)], order='name asc')
            else:
                partner_ids = self.env['res.partner'].search(['|',('is_laboratory', '=', True),('is_matriculado', '=', True),('city','=',ciudad_view)], order='city asc')
        if (select_type=='Matriculados'):
            if (todas_las_ciudades):
                partner_ids = self.env['res.partner'].search([('is_matriculado', '=', True)], order='name asc')
            else:
                partner_ids = self.env['res.partner'].search([('is_matriculado', '=', True),('city','=',ciudad_view)], order='city asc')

        if (select_type=='Laboratorios'):
            if (todas_las_ciudades):
                partner_ids = self.env['res.partner'].search([('is_laboratory', '=', True)], order='name asc')
            else:
                partner_ids = self.env['res.partner'].search([('is_laboratory', '=', True),('city','=',ciudad_view)], order='city asc')

        return partner_ids



    @api.multi
    def getDebtPartner (self,partner_ids,start_date,total_history):
        invoice_all=[]
        if total_history:
            start_date = ""
        for partner in partner_ids:
            if (total_history):
                invoice_ids = self.env['account.invoice'].search([('partner_id', '=' ,partner.id), ('residual', '>', 0.0)], order='date_invoice')
            else:
                invoice_ids = self.env['account.invoice'].search(['&',('partner_id', '=' ,partner.id), ('residual', '>', 0.0), ('date_due','>=', start_date)], order='date_invoice')
            if invoice_ids:

                total= self.getTotalDebt(invoice_ids)
                first_fees = self.getFirstDebt(invoice_ids)
                last_fees = self.getLastDebt(invoice_ids)
                l = datetime.strptime(last_fees.date_invoice, '%Y-%m-%d')
                ld = l.strftime('%d-%m-%Y')
                f = datetime.strptime(first_fees.date_invoice, '%Y-%m-%d')
                fd = l.strftime('%d-%m-%Y')
                invoice_all.append((0,0,{
                    'partner_matricula': partner.matricula_number,
                    'partner_name': partner.name,
                    'total' : total, 
                    'first_fees': first_fees.display_name,
                    'last_fees': last_fees.display_name,
                    'first_date_invoice':fd,
                    'last_date_invoice':ld, 
                }))

        return invoice_all

    @api.multi
    def getTotalDebt(self, invoice_ids):
        total = 0.0
        for invoice in invoice_ids:
            total = invoice.residual + total
        return total
    
    
    @api.multi
    def getFirstDebt(self,invoice_ids):
        invoice = invoice_ids[0]
        return invoice 
            
    @api.multi
    def getLastDebt(self,invoice_ids):
        invoice = invoice_ids[-1]
        return invoice

    @api.model
    def get_report_values(self, docids, data=None):
        select_type = data['form']['select_type']
        todas_las_ciudades = data['form']['todas_las_ciudades']
        ciudad_view = data['form']['ciudad_view']
        state_view = data['form']['state_view']
        start_date = data['form']['start_date']
        total_history = data['form']['total_history']
        partner_ids = self.getPartner(select_type,todas_las_ciudades,ciudad_view)
        invoice_debt = self.getDebtPartner(partner_ids,start_date,total_history)
        title = "Reporte de Deudas"
        if total_history:
            ds = "Todos los tiempos"
        else:
            d = datetime.strptime(start_date, '%Y-%m-%d')
            ds = d.strftime('%d-%m-%Y')

        return {
            'doc_ids': data['ids'],
            'doc_model': data['model'],
            'docs' : invoice_debt,
            'state_view' : state_view,
            'title': title,
            'start_date':ds,
            'select_type':select_type,
        }

    