# -*- coding: utf-8 -*-

##############################################################################

#

#    Copyright (C) 2017 GL (<https://ar.linkedin.com/lucianogenessini>).

#

##############################################################################
from odoo import api, fields, models, _
##############################################################################
from datetime import date
from datetime import datetime
from dateutil import relativedelta
import time
from odoo.exceptions import ValidationError

class res_partner(models.Model):

    _name = 'res.partner'
    _inherit = 'res.partner'

    is_matriculado = fields.Boolean(string='Matriculado')
    is_laboratory = fields.Boolean(string='Laboratorio')
    birthdate = fields.Date(string='Fecha de Nacimiento')
    matricula_number = fields.Integer(string='N° de Matrícula')
    matricula_date = fields.Date(string='Fecha de Inscripción')
    marital = fields.Selection(string='Estado Civil', selection=[('single', 'Soltero/a'),
                                                                 ('married', 'Casado/a'),
                                                                 ('divorced', 'Divorciado/a'),
                                                                 ('widower', 'Viudo/a')],
                               default='single')
    age = fields.Char(string='Edad',
        store=False, readonly=False, compute='_partner_age_fnt')
    dni = fields.Char(string='DNI', size=8)
    city_id = fields.Many2one('colbio.ciudad', string='Ciudad')
    #datos laborales
    job_street = fields.Char()
    job_city = fields.Many2one('colbio.ciudad', string='Ciudad')
    job_phone = fields.Char(string="Teléfono Profesional")
    certificaciones = fields.Selection([('certifica','Certifica'),
    	                     ('no_certifica','No Certifica'),
                             ('recertifica','Recertifica')], string='Certificaciones', default='certifica')
    #datos de estudio
    estudio_ids = fields.One2many('colbio.estudio', 'partner_id', string='Estudios')
    curso_ids = fields.One2many('colbio.estudio', 'partner_curso_id', string='Capacitaciones')
    idioma_ids = fields.Many2many('colbio.idioma', string='Idiomas',  ondelete='restrict')
    profesion_id = fields.Many2one('colbio.profesion', string='Profesión', ondelete='restrict')
    #datos de familiares
    familiar_ids = fields.One2many('colbio.familiar', 'partner_id', string='Familiares')
    state = fields.Selection([('pendiente','Pendiente'),
    	                     ('activo','Activo'),
                             ('advertido','Advertido'),
    	                     ('suspendido','Suspendido'),
    	                     ('baja','Baja')], string='Status', default='pendiente', readonly=True )
    #Datos sobre los motivos de la última suspensión o de la última baja para ser mostrado en el res_partner
    issue_lines = fields.One2many('colbio.matriculado.issue', 'partner_id', help="Incidencias.")
    motivo_cambio_estado_id =fields.Many2one('colbio.motivo',string='Motivo del Estado')
    date_cambio_estado = fields.Date(string='Fecha del último Estado')
    #Datos sobre empleo o actividades profesionales
    empleo_ids = fields.One2many('colbio.empleo', 'partner_id', string='Cargos o empleos')
    #Datos sobre los aportes
    obra_social_id = fields.Many2one('colbio.obra.social', string='Obra Social', ondelete='restrict')
    caja_jubilatoria_id = fields.Many2one('colbio.caja.jubilatoria', string='Caja Jubilatoria', ondelete='restrict')
    #Para filtrar por donde cobran las cuotas de los matriculados
    cobra_por = fields.Selection([('circulo','Circulo'),
                             ('unam','UNaM'),
                             ('colegio','Colegio'),
                             ], string='Cobra por:', default='colegio')
  

            
    @api.onchange('state_id')
    def _onchange_state_id(self):
        if self.state_id:
            return {'domain': {'city_id': [('state_id', '=', self.state_id.id)],'job_city': [('state_id', '=', self.state_id.id)]}, 
            'value': {'country_id': self.state_id.country_id}}
        else:
            return {'domain': {'city_id': [], 'job_city':[] }}

    @api.onchange('city_id')
    def _onchange_city_id(self):
        if self.city_id.id:
            country = self.city_id.state_id.country_id
            self.city = self.city_id.name
            return {'value': {'state_id': self.city_id.state_id, 'zip':self.city_id.codigo_postal, 'country_id' : country}}
            print ("---------------------------------------------------_>>>>", city)

    @api.onchange('country_id')
    def _onchange_country_id(self):
        if self.country_id:
            return {'domain': {'state_id': [('country_id', '=', self.country_id.id)]}}
        else:
            return {'domain': {'state_id': []}}

    @api.constrains('matricula_number')
    def _check_matricula_number(self):
        for record in self:
            if record.matricula_number == 0 and self.state == 'pendiente':
                return True
            if record.matricula_number == 0 and self.state != 'pendiente':
                raise ValidationError("No se puede asignar 0 a un matriculado que este en un estado distinto a pendiente.")
            else:
                partner = self.env['res.partner'].search([('matricula_number', '=', record.matricula_number)])
                if partner.id != record.id:
                    raise ValidationError("Ya existe un matriculado con la matrícula %s" % record.matricula_number)
                else:
                    return True

    @api.one
    @api.depends('birthdate')
    def _partner_age_fnt(self):
        def compute_age_from_dates (partner_dob, patient_deceased=False, patient_dod=False):
            now = datetime.now()
            if (partner_dob):
                dob = datetime.strptime(partner_dob, '%Y-%m-%d')
                if patient_deceased :
                    dod = datetime.strptime(patient_dod, '%Y-%m-%d %H:%M:%S')
                    delta = relativedelta.relativedelta(dod, dob)
                    deceased = " (deceased)"
                else:
                    delta = relativedelta.relativedelta(now, dob)
                    deceased = ''
                years_months_days = str(delta.years)
            else:
                years_months_days = "Fecha de nacimiento no asignada !"

            return years_months_days
        result = {}
        # for partner_data in self.browse(cr, uid, ids, context=context):
        partner_data = self
        self.age= compute_age_from_dates (partner_data.birthdate)

    def print_carnet(self):
        datas = {
            'ids': [self.id],
            'model': 'res.partner',
            'form': self.id,
        }
        ids_ret = [self.id]
        ctx = dict(self.env.context)
        ctx.update({'active_id': ids_ret[0],
                    'active_ids': ids_ret,
                    'active_model': 'res.partner',
                   })
        return {
            'type': 'ir.actions.report',
            'report_name': 'carnet.matriculado',
            'report_type': 'aeroo',
            'datas': datas,
            'context': ctx,
            'target': 'current',
            }

    def get_matricula(self):
        matriculados = self.env['res.partner'].search([('is_matriculado', '=', True)], order='matricula_number desc', limit=1)
        mat = matriculados.matricula_number + 1
        return mat


    #Genera el código secuencial para el laboratorio
    def get_cod_lab(self):
        laboratorios = self.env['res.partner'].search([('is_laboratory', '=', True)], order='cod_number_lab desc', limit=1)
        print ('########################', laboratorios.cod_number_lab)
        cod = laboratorios.cod_number_lab + 1
        print ('########################', cod)
        return cod


    @api.multi
    def button_alta(self):
        #Cambiar al estado activo
        if self.state == 'pendiente':
            self.write({'state': 'activo'})
            if (self.is_matriculado):
                #Asigna el número de matricula
                self.matricula_number = self.get_matricula()
            if (self.is_laboratory):
                #Asigna el número de laboratorio
                self.cod_number_lab = self.get_cod_lab()
                print ('-------------------------------------_>', self.get_cod_lab())
            #Cargar la fecha del dia en la fecha de inscripcion de la matricula
            self.matricula_date = datetime.now()
        if self.state == 'suspendido':
            self.write({'state': 'activo'})
            self.motivo_cambio_estado_id = None
            issue_obj = self.env['colbio.matriculado.issue']
            issue_obj.create({
                'partner_id': self.id,
                'descripcion': "Cambio de estado Suspendido a Activo",
                'motivo_id': 0,
                'date_issue': datetime.now(),
                'state':'activo'})
        if self.state == 'advertido':
            self.write({'state': 'activo'})
            self.motivo_cambio_estado_id = None
            issue_obj = self.env['colbio.matriculado.issue']
            issue_obj.create({
                'partner_id': self.id,
                'descripcion': "Cambio de estado Advertido a Activo",
                'motivo_id': 0,
                'date_issue': datetime.now(),
                'state':'activo'})
        return {}

    @api.multi
    def button_suspendido(self,motivo_id,descripcion,partner_id):
        self.write({'state': 'suspendido'})
        self.motivo_cambio_estado_id = motivo_id
        self.date_cambio_estado = datetime.now()
        issue_obj = self.env['colbio.matriculado.issue']
        issue_obj.create({
            'partner_id': partner_id,
            'descripcion': descripcion,
            'motivo_id': motivo_id[0],
            'date_issue': datetime.now(),
            'state':'suspension'})
        return {}

    @api.multi
    def button_advertido(self,motivo_id,descripcion,partner_id):
        self.write({'state': 'advertido'})
        self.motivo_cambio_estado_id = motivo_id
        self.date_cambio_estado = datetime.now()
        issue_obj = self.env['colbio.matriculado.issue']
        issue_obj.create({
            'partner_id': partner_id,
            'descripcion': descripcion,
            'motivo_id': motivo_id[0],
            'date_issue': datetime.now(),
            'state':'advertido'})
        return {}

    @api.multi
    def button_baja(self, motivo_id, descripcion,partner_id):
        self.write({'state': 'baja'})
        self.motivo_cambio_estado_id = motivo_id
        self.date_cambio_estado = datetime.now()
        issue_obj = self.env['colbio.matriculado.issue']
        issue_obj.create({
            'partner_id': partner_id,
            'descripcion': descripcion,
            'motivo_id': motivo_id[0],
            'date_issue': datetime.now(),
            'state':'baja'})
        return {}

    #Método automatizado para el envío de mails automatizados
    @api.multi
    def send_automatic_email_deuda(self):
        today = datetime.now().date()
        #Cantidad de meses necesarios para enviar mail de deuda
        number_months_debt = 3
        res_partners = self.env['res.partner'].search([('state', '=', 'activo'), ('is_matriculado', '=', True)]) 
        for partner in res_partners:
                invoice = self.env['account.invoice'].search([('partner_id', '=', partner.id), ('state', '=', 'paid')], order='date_due desc', limit=1)
                if invoice:
                    if invoice.date_due < today.strftime('%Y-%m-%d'):
                        date_due = datetime.strptime(invoice.date_due , '%Y-%m-%d')
                        month_diff = abs( int(today.strftime("%m")) - int(date_due.strftime("%m") ))
                        if month_diff > number_months_debt:
                            #Envia el Mail
                            template = self.env.ref('colbio.aviso_deuda_template')
                            self.env['mail.template'].browse(template.id).send_mail(partner.id, force_send=True)
                            if partner.state == 'activo': 
                                #Cambia de estado al partner
                                partner.write({'state': 'suspendido'})
                                descripcion = 'Cambio de estado automático por adeudar cuotas'
                                motivo_cambio_estado = self.env['colbio.motivo'].search([('name', '=', 'Falta de pago')])
                                print(motivo_cambio_estado.id)
                                partner.date_cambio_estado = datetime.now()
                                issue_obj = self.env['colbio.matriculado.issue']
                                issue_obj.create({
                                    'partner_id': partner.id,
                                    'descripcion': descripcion,
                                    'motivo_id': motivo_cambio_estado.id,
                                    'date_issue': datetime.now(),
                                    'state':'suspension'})

   #Dejo por defecto como tipo de documento al CUIT
    def _get_default_category(self):
        res = self.env['res.partner.id_category'].search([('code', '=', 'CUIT')])
        return res and res[0] or False
    main_id_category_id = fields.Many2one('res.partner.id_category', required=False, default=_get_default_category)

    #Dejo por defecto como responsabilidad de afip a Consumidor Final
    def _get_default_category_afip(self):
        res = self.env['afip.responsability.type'].search([('code', '=', '5')])
        return res and res[0] or False
    afip_responsability_type_id = fields.Many2one('afip.responsability.type', required=False, default=_get_default_category_afip)

    ####################################----LABORATORIO----#######################################
    laboratory_m2m_ids = fields.Many2many ( "res.partner", "rel_table", "direct_rel_id", "back_rel_id", string = "Laboratorios") 
    matricualdos_m2m_ids = fields.Many2many ( "res.partner", "rel_table", "back_rel_id", "direct_rel_id",    string = "Matricualdos")
    dt= fields.Many2one('res.partner', index=True, string="Director Técnico", default=False)
    lab = fields.Many2one('res.partner', 'Director Técnico de:')
    adjunto_ids= fields.Many2many(comodel_name="ir.attachment", relation="m2m_ir_adjunto_rel", column1="m2m_id", column2="attachment_id", string="Adjuntos")
    technical_staff = fields.Selection(string='¿Posee personal técnico?', selection=[('si', 'SI'), ('no', 'NO')])
    cleaning_staff = fields.Selection(string='¿Posee personal de limpieza?', selection=[('si', 'SI'), ('no', 'NO')])
    samples_extraction = fields.Selection(string='¿Realiza extración de muestras a domicilio?', selection=[('si', 'SI'), ('no', 'NO')])
    dependencia = fields.Selection(string='Dependencia', selection=[('provincial', 'Provincial'), ('municipal', 'Municipal'), ('privada', 'Privada'), ('otras', 'Otras')])
    turno = fields.Selection(string='Turno:', selection=[('mañana', 'Turno Mañana'), ('tarde', 'Turno Tarde'), ('ambos', 'Mañana y Tarde')])
    weeklyholiday = fields.Many2many('colbio.dias.semana', string="Días en los que atiende")
    
    instrumentos_ids = fields.One2many('colbio.instrumento.laboratorio', 'partner_id', string='Instrumentos Generales')
    instrumentos_microbiologia_ids = fields.One2many('colbio.instrumento.laboratorio', 'partner_microbiologia_id',string='Instrumentos de Microbilogía')
    
    practicas_m2m_ids = fields.Many2many('colbio.practica', string="Prácticas")
    laboratorio_externo_m2m_ids = fields.Many2many('colbio.laboratorio.externo', string="Laboratorio externo")
    cod_number_lab = fields.Integer(string="N° de Laboratorio")
    owner_ids = fields.One2many('colbio.dueno.laboratorio', 'partner_id', string='Dueño')


    @api.constrains('cod_number_lab')
    def _check_cod_number_lab(self):
        for record in self:
            if record.cod_number_lab == 0 and self.state == 'pendiente':
                return True
            if record.cod_number_lab == 0 and self.state != 'pendiente':
                raise ValidationError("No se puede asignar 0 como código a un laboratorio que este en un estado distinto a pendiente.")
            else:
                partner = self.env['res.partner'].search([('cod_number_lab', '=', record.cod_number_lab)])
                if partner.id != record.id:
                    raise ValidationError("Ya existe un laboratorio con con el mismo código %s" % record.cod_number_lab)
                else:
                    return True

    #No permite que un matriculado sea director técnico de mas de un laboratorio. 
    @api.constrains('dt')
    def _check_dt_unique(self):
        if self.is_matriculado == True: return True      
        res = self.env['res.partner'].search([('is_laboratory', '=', True)])
        for r in res:
            if r.dt:   
                if r.dt.id == self.dt.id and r.id != self.id: 
                    raise ValidationError("El director técnico que está intentando asignar ya se encuentra asignado al laboratorio %s" %r.name)
         #Acá asigno el laboratorio recien creado al director técnico para que sea mostrado en matriculados
        if self.dt: self.dt.lab = self.id
        return True
        
class MatriculdoIssue(models.Model):

    _name = 'colbio.matriculado.issue'
    partner_id = fields.Many2one('res.partner', 'Matriculado')
    descripcion = fields.Char(string='Descripcion del Cambio')
    motivo_id =fields.Many2one('colbio.motivo',string='Motivo del Cambio')
    date_issue = fields.Date(string='Fecha del Cambio')
    state = fields.Selection(string='Estado del Matriculado', selection=[('baja', 'Baja'),
                                                      ('advertido','Advertido'),
                                                      ('suspension', 'Suspensión'),
                                                      ('activo', 'Activo')],
                               default='baja', required=True)

