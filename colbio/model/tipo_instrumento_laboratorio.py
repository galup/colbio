# -*- coding: utf-8 -*-
from odoo import models, fields, api, _

class colbio_tipo_instrumento(models.Model):

	_name= 'colbio.tipo.instrumento'

	name = fields.Char(string='Tipo de Instrumento')

	_sql_constraints = [
		('name_uniq', 'UNIQUE (name)',  'Ya existe un tipo de instrumento con el mismo nombre')
	]