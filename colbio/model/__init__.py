# -*- coding: utf-8 -*-

##############################################################################

#



#

##############################################################################

from . import res_partner
from . import universidad
from . import nivel
from . import estudio_titulo
from . import idioma
from . import estudio
from . import profesion
from . import familiar
from . import motivo
from . import empleo
from . import ciudad
from . import sector_laboratorio
from . import practica_laboratorio
from . import tipo_instrumento_laboratorio
from . import marca_instrumento_laboratorio
from . import instrumento_laboratorio
from . import laboratorio_externo
from . import dias_semana
from . import obra_social
from . import caja_jubilatoria
from . import dueno_laboratorio
from . import product_template
from . import account_invoice
