# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

#Se definen las cajas jubilatorias para los matriculados
class colbio_caja_jubilatoria(models.Model):

    _name= 'colbio.caja.jubilatoria'

    name = fields.Char(string='Caja Jubilatoria', required=True)
    tipo = fields.Selection(string='Tipo', selection=[('nacional', 'Nacional'),
    													('provincial', 'Provincial')],
                                required=True)