# -*- coding: utf-8 -*-
# --------------------------------------------------------------------------
#
#Reporte que imprime un listado de los matriculados según su estado de activo, suspendido, baja
#
# ---------------------------------------------------------------------------

from odoo import models, fields, api



class ReportMatriculadoWizard(models.TransientModel):
    _name = 'matriculado.report.wizard'

    state_view = fields.Selection([('todos', 'Todos'),
                             ('pendiente','Pendiente'),
                             ('activo','Activo'),
                             ('suspendido','Suspendido'),
                             ('baja','Baja')], string='Status', default='todos', required=True)


    @api.multi
    def get_report(self):
        data = {
            'ids': self.ids,
            'model': 'self._name',
            'form': {
                'state_view': self.state_view,
            },

        }
        return self.env.ref('colbio.reporte_matriculado').report_action(self, data=data)

class ReportAttendanceColbio(models.AbstractModel):
    """Abstract Model for report template.
    for `_name` model, please use `report.` as prefix then add `module_name.report_name`.
    """

    _name = 'report.colbio.report_matriculado'

    @api.model
    def get_report_values(self, docids, data=None):
        state_view = data['form']['state_view']
        print('asd', state_view)
        docs = []
        if (state_view != 'todos'):
            matriculados = self.env['res.partner'].search([('state','=',state_view)], order='name asc')
            matriculados_count = len(matriculados)
            if (state_view == 'activo'):
                title = "Reporte de Matriculados Activos"
            if (state_view == 'suspendido'):
                title = "Reporte de Matriculados Suspendidos"
            if (state_view == 'baja'):
                title = "Reporte de Matriculados con Baja"
            if (state_view == 'pendiente'):
                title = "Reporte de Matriculados Pendientes"
        else:
            matriculados = self.env['res.partner'].search([('is_matriculado', '=', True)], order='name asc')
            matriculados_count = len(matriculados)
            title = "Reporte de Matriculados"
        return {
            'doc_ids': data['ids'],
            'doc_model': data['model'],
            'docs': matriculados,
            'matriculados_count': matriculados_count,
            'state_view': state_view,
            'title': title,
        }
