# -*- coding: utf-8 -*-
from odoo import models, fields, api, _

class colbio_empleo(models.Model):

	_name= 'colbio.empleo'

	#name = fields.Char(string='Particular')		   
	particular_id = fields.Many2one('colbio.particular', 'Cargo particular')
	especialidad_id = fields.Many2one('colbio.especialidad', 'Especialidad a la que se dedica')
	actuacion_cientifica = fields.Char(string='Actuación científica')
	actuacion_didactica = fields.Char(string='Actuación didáctica')
	actuacion_entidades = fields.Char(string='Actuación en entidades profesionales')
	partner_id = fields.Many2one('res.partner', 'Matriculado')




class colbio_especialidad(models.Model):
	_name='colbio.especialidad'

	name = fields.Char(string='Especiaidad a la que se dedica')

class colbio_particular(models.Model):
	_name='colbio.particular'

	name = fields.Char(string='Cargo Particular')