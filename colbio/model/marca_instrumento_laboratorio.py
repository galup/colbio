# -*- coding: utf-8 -*-
from odoo import models, fields, api, _

class colbio_marca_instrumento(models.Model):

	_name= 'colbio.marca.instrumento'

	name = fields.Char(string='Marcas del Instrumento')

	_sql_constraints = [
		('name_uniq', 'UNIQUE (name)',  'Ya existe una marca de instrumento con el mismo nombre')
	]