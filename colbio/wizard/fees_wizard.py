# -*- coding: utf-8 -*-
# --------------------------------------------------------------------------
#
#Reporte que imprime un listado de los matriculados según su estado de activo, suspendido, baja
#
# ---------------------------------------------------------------------------

from odoo import models, fields, api
import re
from calendar import monthrange
from datetime import datetime, timedelta
from odoo.tools.translate import _
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, \
    DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import except_orm
from odoo.exceptions import ValidationError
from dateutil.relativedelta import relativedelta



class FeesWizard(models.TransientModel):
    _name = 'fees.wizard'

    select_type = fields.Selection(string="Facturar a:",
                                   selection=[('matriculado', 'Matriculado'),
                                              ('laboratorio', 'Laboratorio')],
                                   default='matriculado')
    matriculado = fields.Many2many(comodel_name="res.partner",
                                   relation="m2m_ir_partner_rel",
                                   column1="wizard_id",
                                   column2="partner_id", string="Matriculados")
    laboratorio = fields.Many2many(comodel_name="res.partner",
                                   relation="m2m_ir_partner_rel",
                                   column1="wizard_id", column2="partner_id", string="Laboratorios")
    start_date = fields.Date(string="Fecha Desde", required=True)
    end_date = fields.Date(string="Fecha Hasta", required=True)
    product_ids = fields.Many2many(comodel_name="product.template",
                                   relation="m2m_ir_product_rel",
                                   column1="wizard_id", column2="product_id", string="Conceptos")
    invoice_per_month = fields.Boolean(string="Crear una factura por cada mes", default=True)
    tax_id = fields.Many2one('account.tax', string="Impuesto")
    info = fields.Text()
    state = fields.Selection([('choose', 'choose'),
                              ('get', 'get'),
                              ('done', 'done'),
                              ('error', 'error')], default=lambda *a: 'choose')
    invoice_open = fields.Boolean(string="Aprobar facturas", default=True)

    def get_dict(self, wizard, view_id):
        return {
            'res_id': wizard.id,
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': view_id.id,
            'target': 'new',
            'nodestroy': True,
        }

    @api.multi
    def add_line_ids(self, ds):
        invoice_line_ids = []
        #Se generan las líneas para cargar en la factura
        for product in self.product_ids:
            name_line = "Corresponde al pago de %s, (%s de %s)" % (str(product.name),
                                                                   str(ds.strftime('%B')),
                                                                   str(ds.strftime('%Y')))
            invoice_line_ids.append((0, 0, {
                'product_id':product.id,
                'quantity': 1,
                'price_unit': product.lst_price,
                'name': name_line,
                'account_id': 1,
                #'invoice_line_tax_ids': [(6, 0, [self.tax_id.id])]
                }))
        return invoice_line_ids

    @api.multi
    def generate_invoice(self):
        invoice_ids = []
        if (self.start_date < self.end_date and
                self.product_ids and (self.matriculado or self.laboratorio)):
            interval = 1
            invoice_obj = self.env['account.invoice']
            msg = ""
            immediate_payment_id = (self.env['account.payment.term'].search([('name', '=', 'Pago inmediato')])).id

            if self.matriculado:
                partners = self.matriculado
            else:
                partners = self.laboratorio
            for partner in partners:
                invoice_id = False
                for data in self:
                    #Condición para saber si se hace una factura por cada mes
                    if data.invoice_per_month:
                        ds = datetime.strptime(data.start_date, '%Y-%m-%d')
                        while ds.strftime('%Y-%m-%d') < data.end_date:
                            de = ds + relativedelta(months=interval, days=-1)
                            if de.strftime('%Y-%m-%d') > data.end_date:
                                de = datetime.strptime(data.end_date, '%Y-%m-%d')

                            year = ds.strftime('%Y')
                            month = ds.strftime('%m')
                            invoice_vals = {
                                'partner_id': partner.id,
                                'date_invoice': ds.strftime('%Y-%m-%d'),
                                'date_due': de.strftime('%Y-%m-%d'),
                                'year': year,
                                'month': month,
                                'payment_term_id': immediate_payment_id,
                                'afip_service_start': ds.strftime('%Y-%m-%d'),
                                'afip_service_end': de.strftime('%Y-%m-%d'),
                                #'journal_id':9

                            }

                            invoice_line_ids = self.add_line_ids(ds)
                            invoice_vals.update({'invoice_line_ids':invoice_line_ids})
                            if not invoice_obj.search_invoice_by_period(month, year, partner):
                                invoice_id = invoice_obj.create(invoice_vals)
                                invoice_ids.append(invoice_id.id)
                            else:
                                msg += "%s\n"%invoice_obj.get_invoice_msg(month, year, partner)
                            ds = ds + relativedelta(months=interval)
                    else:
                        ds = datetime.strptime(data.start_date, '%Y-%m-%d')
                        de = datetime.strptime(data.end_date, '%Y-%m-%d')
                        #Calcular la cantidad de meses que existe entre las fechas para modificar las cantidades en cada línea de la factura
                        month_diff = (monthdelta(ds, de)+1)
                        invoice_vals = {
                            'partner_id': partner.id,
                            'date_invoice': ds.strftime('%Y-%m-%d'),
                            'date_due': de.strftime('%Y-%m-%d'),
                            'payment_term_id': immediate_payment_id,
                            'afip_service_start': ds.strftime('%Y-%m-%d'),
                            'afip_service_end': de.strftime('%Y-%m-%d'),
                            #'journal_id':9,
                            }
                        invoice_line_ids = []
                        ds_mes = datetime.strptime(self.start_date, '%Y-%m-%d')
                        ds_mes_hasta = datetime.strptime(self.end_date, '%Y-%m-%d')

                        for product in self.product_ids:
                            name_mes = """Corresponde al pago de %s ( %s de %s a %s de %s) \
                                        """ % (str(product.name),
                                               str(ds_mes.strftime('%B')),
                                               str(ds_mes.strftime('%Y')),
                                               str(ds_mes_hasta.strftime('%B')),
                                               str(ds_mes_hasta.strftime('%Y')))
                            if month_diff < 2:
                                name_mes = """Corresponde al pago de %s \
                                           (%s de %s)""" % (str(product.name),
                                                            str(ds_mes.strftime('%B')),
                                                            str(ds_mes.strftime('%Y')))
                            invoice_line_ids.append((0, 0, {
                                'product_id':product.id,
                                'quantity': month_diff,
                                'price_unit': product.lst_price,
                                'name': name_mes,
                                'account_id': 1,
                                }))
                        invoice_vals.update({'invoice_line_ids':invoice_line_ids})
                        invoice_id = invoice_obj.create(invoice_vals)
                        invoice_ids.append(invoice_id.id)
                    if data.invoice_open and invoice_id:
                        invoice_id.action_invoice_open()
        else:
            if self.start_date >= self.end_date:
                raise ValidationError("""La fecha de vencimiento debe ser mayor \
                                         a la fecha de la factura.""")
            if not self.product_ids:
                raise ValidationError("Debe existir al menos un linea de concepto cargada.")
            if not (self.matriculado and self.laboratorio):
                raise ValidationError("Debe agregar al menos un matrculado o laboratorio.")

        ## si está seteado msg se muestra wizard con mensajes de error, sino directamente se va a la vista de facturas
        if msg:
            self.write({'state':'get', 'info':msg})
            view_id = self.env['ir.ui.view'].search([('model', '=', 'fees.wizard')])
            ret = self.get_dict(self, view_id)
            ret['type'] = 'ir.actions.act_window'
            ret['res_model'] = 'fees.wizard'
            ret['name'] = _('Generar Cuota')
            ret['context'] = self._context
        else:
            ret = self.view_invoice(invoice_ids)
        return ret

    @api.multi
    def view_invoice(self, invoice_ids):
        return {
            'name': 'Invoices',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.invoice',
            'view_id': False,
            'views': [(self.env.ref('account.invoice_tree').id, 'tree'), (self.env.ref('account.invoice_form').id, 'form')],
            'context': "{'type':'out_invoice'}",
            'type': 'ir.actions.act_window',
            'domain': "[('id','in', [" + ','.join(map(str, invoice_ids)) + "])]",
            }


def monthdelta(d1, d2):
    delta = 0
    while True:
        mdays = monthrange(d1.year, d1.month)[1]
        d1 += timedelta(days=mdays)
        if d1 <= d2:
            delta += 1
        else:
            break
    return delta
