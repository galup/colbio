# -*- coding: utf-8 -*-
from odoo import models, fields, api, _

class colbio_profesion(models.Model):

	_name= 'colbio.profesion'
			   
	name = fields.Char(string='Nombre', required=True)

	_sql_constraints = [
		('name_uniq', 'UNIQUE (name)',  'Ya existe una profesión con el mismo nombre')
	]