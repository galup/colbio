##############################################################################
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
##############################################################################

class account_invoice(models.Model):

    _name = 'account.invoice'
    _inherit = 'account.invoice'

    number_fee = fields.Char(string='N° de Factura')
    month = fields.Integer()
    year = fields.Integer()

    def search_invoice_by_period(self, month, year, partner_id):
        args = [('year', '=', year),
                ('month', '=', month),
                ('partner_id', '=', partner_id.id),
               ]
        if self.id:
            args.append(('id', '<>', self.id))
        invoice_ids = self.search(args)
        if invoice_ids:
            return True
        return False

    def get_invoice_msg(self, month, year, partner_id):
        return "Ya existe una factura para el periodo %s/%s-%s."%(month,
                                                                  year,
                                                                  partner_id.name,
                                                                 )

    @api.one
    @api.constrains('month', 'year', 'partner_id')
    def _check_invoice(self):
        if self.month and self.year and self.partner_id:
            if self.search_invoice_by_period(self.month, self.year, self.partner_id):
                msg = self.get_invoice_msg(self.month, self.year, self.partner_id)
                raise ValidationError(_(msg))
