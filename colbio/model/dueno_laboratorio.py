from odoo import models, fields, api, _

class colbio_dueno_laboratorio(models.Model):

	_name= 'colbio.dueno.laboratorio'

	name = fields.Char(string='Nombre', required=True)
	cuit = fields.Char(string='Cuit')
	partner_id = fields.Many2one('res.partner', 'Laboratorio', ondelete='restrict')
