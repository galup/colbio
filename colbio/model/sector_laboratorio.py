# -*- coding: utf-8 -*-
from odoo import models, fields, api, _

class colbio_sector(models.Model):

	_name= 'colbio.sector'

	name = fields.Char(string='Sector')

	_sql_constraints = [
		('name_uniq', 'UNIQUE (name)',  'Ya existe un sector con el mismo nombre')
	]