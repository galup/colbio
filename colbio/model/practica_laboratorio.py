# -*- coding: utf-8 -*-
from odoo import models, fields, api, _

class colbio_practica(models.Model):

	_name= 'colbio.practica'

	name = fields.Char(string='Práctica')

	_sql_constraints = [
		('name_uniq', 'UNIQUE (name)',  'Ya existe una práctica con el mismo nombre')
	]