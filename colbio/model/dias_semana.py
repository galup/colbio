# -*- coding: utf-8 -*-
from odoo import models, fields, api, _

class DiasSemana (models.Model):
    _name = 'colbio.dias.semana'
    name = fields.Char()