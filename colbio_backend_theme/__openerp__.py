# -*- coding: utf-8 -*-

{
    "name": "ColBio Backend Theme",
    "summary": "ColBio backend theme",
    "version": "9.0.6",
    "category": "Themes/Backend",
    "website": "http://www.galup.com.ar",
    "description": """
                Backend theme for Odoo 11.0 ColBio.
    """,
    'depends': ['base', 'web_responsive'],
    'data': ['views.xml',],
    'application': True,
    'auto_install': False,
    'installable': True,
}
