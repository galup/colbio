# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import ValidationError

class colbio_instrumento(models.Model):

	_name= 'colbio.instrumento'

	name = fields.Char(string='Instrumento')
	marca_id = fields.Many2one('colbio.marca.instrumento', string='Marca del Instrumento')
	tipo_instrumento_id = fields.Many2one('colbio.tipo.instrumento', string='Tipo de Instrumento')
	is_microbiologia = fields.Boolean(string='¿Es de Microbiología?')

class colbio_instrumento_laboratorio(models.Model):
	_name='colbio.instrumento.laboratorio'

	instrumento_id = fields.Many2one('colbio.instrumento', string='Instrumento')
	sector_id = fields.Many2one('colbio.sector', string='Sector')
	cantidad = fields.Integer(string='Cantidad')
	descripcion = fields.Char(string='Descripción')
	partner_id = fields.Many2one('res.partner', 'Laboratorio', ondelete='restrict')
	partner_microbiologia_id = fields.Many2one('res.partner', 'Laboratorio', ondelete='restrict')
	numero_serie = fields.Char(string='Número de Serie')


	@api.constrains('numero_serie')
	def _check_numero_serie_unique(self):
		for record in self:
			if record.numero_serie is None: return True
			instrumento = self.env['colbio.instrumento.laboratorio'].search([('numero_serie', '=', record.numero_serie)], limit=1)
			if instrumento.id != record.id:
				if(instrumento.partner_id):
				    raise ValidationError("El número de serie se encuentra registrado en el laboratorio %s" %instrumento.partner_id.name)
				else:
					raise ValidationError("El número de serie se encuentra registrado en el laboratorio %s" %instrumento.partner_microbiologia_id.name)
			else:
				return True